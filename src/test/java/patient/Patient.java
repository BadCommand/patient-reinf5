package patient;

public class Patient {

	String name;
	int height;
	double weight;
	double temperature;
	boolean isVaccsOk;

	public Patient(String name) {
		this.name = name;
	}

	public void addCheckup(int height, double weight, double temp, boolean vaccs) {
		this.height = height;
		this.weight = weight;
		this.temperature = temp;
		this.isVaccsOk = vaccs;
	}

	public String toString() {

		String infos = String.format("Name: %s,Height: %s,Weight: %s,Temperature: %s", name, height, weight,
				temperature);

		return infos;
	}
}
