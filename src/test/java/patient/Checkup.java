package patient;

public class Checkup {

	int height = 0;
	double weight = 0;
	double temperature = 0;
	boolean isVaccsOk = false;

	public Checkup(int height, double weight, double temperature, boolean isVaccsOk) {
		this.height = height;
		this.weight = weight;
		this.temperature = temperature;
		this.isVaccsOk = isVaccsOk;

	}

	public int getHeight() {

		return height;
	}

	public double getWeight() {

		return weight;
	}

	public double getTemperature() {

		return temperature;
	}

	public boolean getVaccsOk() {

		return isVaccsOk;
	}

}
